---
title: Mermaid Tutorial
tags: [Notebooks/Tutorial]
created: '2019-04-05T07:34:07.938Z'
modified: '2019-04-05T10:19:49.720Z'
favorited: true
---

# Mermaid Tutorial

> Write by [**Wyatt Huang**](http://overfit.ml)
Cite from: [Mermaid Web](https://mermaidjs.github.io)
*2019/05/05*
<br>**\*COPTRIGHT NOTICE\***
*Contact before spreading this article, Any repulished without authorizing is illegal*


## Menu

+ [Flow Chart](#flowchart)
  + [Node](#node)
  + [Line](#line)
  + [Special characters](#sc)
  + [Subgraph](#subgraph)
  + [Style It](#style_it)
+ [Sequence Diagram](#sequence)
  + [Participant](#participant)
  + [Line](#line_2)
  + [Activate and Deactivate](#a&d)
  + [Note](#note)
  + [Loop](#loop)
  + [Alt & Opt](a&o)
  + [Style](#style_2)
+ [Gant Graph](#gant)
  + [Head rule](#hr)
  + [Middle rule](#mr)
  + [Content part](#cp)

<div id='flowchart'>

## FlowChart

```md
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

**Example:**
```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

For the grammar, the `TD`here mean from the top to the bottom, you may also use:

```md
Possible directions are:

TB - top bottom
BT - bottom top
RL - right left
LR - left right

TD - same as TB
```

<div id='node'>

### node

```md
graph TD
  id1[Hello World]
```

**Example:**
```mermaid
graph TD
  id1[Hello world]
```
<br>
you may also change the shape of the node by using different symbol, like:
```md
id1[Hello world] //this is the square bracket
id1(Hello world) //square bracket with circle corner
id1((hello world)) //circle bracket
id1>hello world] //sharp left open and square bracket right
id1{hello world} //rhombus shape
```

**Example:**
```mermaid
graph TD
  id1{Hello World}
```

<div id='line'>

### Line

```md
graph LR
    A-->B //line with arrow
    A---B //raw line
    A---|text|B //line with text
    A-->|text|B //arrow line with text
    A-.-B //dot line
    A-.->B //dot arrow line
    A===B //thick line
```

**Example:**

```mermaid
graph LR
  a ==>|loves| b
  a -->|friend| c
  a -.->|parent| d
```

<div id='sc'>

### Special characters that break syntax

```md
graph TD
  a["hello world[by python]"] //just put two quotaion mark
  a["#quot; hello world #quot; [By python]"] //if you want to put quote mark
```

**Example:**
```mermaid
graph TD
  a["#quot; hello world #quot; [By python]"]
```

<div id='subgraph'>

### Subgraph

```mermaid
graph LR
subgraph Xiaoming
  Alex ---|love| Steven
end
subgraph Daming
  David ---|son|Ragner
end
subgraph Zhongming
  Louis ---|worked|Jobs
end
Steven -.->|worked|Louis
```

<div id='style_it'>

### Style it!

> It might be a little bit difficult since it need some CSS skill, if you have NO demand for it, just skiped off

```md
graph LR
    id1(Start)-->id2(Stop)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
```

**Example:**

```mermaid
graph LR
    id1(Start)-->id2(Stop)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
```

So, what if you want to give many node a same style, you can give them a same `class` and give the class a style:

```md
graph LR
    id1(Start)-->id2(Stop)
    class id1,id2 exampleClass
    classDef exampleClass fill:#f9f,stroke:#333,stroke-width:4px
```

**Example:**

```mermaid
graph LR
    id1(Start)-->id2(Stop)
    class id1,id2 exampleClass
    classDef exampleClass fill:#f9f,stroke:#333,stroke-width:4px
```

<div id='sequence'>

## Sequence diagram

> It is a kind of graph which show process of one thing
```mermaid
sequenceDiagram
    Alice->>John: Hello John, how are you?
    John-->>Alice: Great!
```

<div id='participant'>

### Participants

You can easily understand it as the declaration of variables, and if do not declare variables, it is also OK.

```md
sequenceDiagram
  participant wyatt
  participant alex
  participant steven

  wyatt ->> alex: hello
  alex ->> wyatt: thank you
  steven ->> alex: I hate you
  alex ->> steven: why you hate me?
```

**Example:**

```mermaid
sequenceDiagram
  participant wyatt
  participant alex
  participant steven

  wyatt->>alex: hello
  alex->>wyatt: thank you
  steven->>alex: I hate you
  alex->>steven: why you hate me?
```

You may ask that since `participnt` is not necessary, why we use it? The reason is that we can use `participant` to define some complex **String** to the simpler one, like **A** or **B** ans so on...

```md
sequenceDiagram
    participant A as Alice
    participant J as John
    A->>J: Hello John, how are you?
    J->>A: Great!
```

**Example:**

```mermaid
sequenceDiagram
    participant A as Alice
    participant J as John
    A-->J: Hello John, how are you?
    J->>A: Great!
```

<div id='line_2'>

### Line:

> I do not know why the represent rules of line for sequence grah is different from the flow Graph. Therefore, If you want to draw a sequence line, please be careful!

```md
->	Solid line without arrow
-->	Dotted line without arrow
->>	Solid line with arrowhead
-->>	Dotted line with arrowhead
-x	Solid line with a cross at the end (async)
--x	Dotted line with a cross at the end (async)
```

<div id='a&d'>

### Activate and Deactivate

> For the sequnce graph, active and deactive use to describe the start and the end of an object

**Example:**
```mermaid
sequenceDiagram
  participant s as Steven
  participant a as Alex
  s->>a: Can you hear me?
  activate a
  a->>s: Now, I can hear you
  deactivate s
```

<div id='note'>

### Note

```md
Note left of John: Text in note
Note right of John: Text in note
Note over John: Text in note
```

**Example:**

```mermaid
sequenceDiagram
  participant s as steven
  Note right of s: Handsome boy
```

Also, If you want to take note over two participant:
```md
Note over John, Steven: Take note
```

**Example:**

```mermaid
sequenceDiagram
  Note over steven, Alex: They love icecream
```

<div id='loop'>

### Loop

If an event happend all the time, you can use `loop` to represent

```md
sequenceDiagram
  participant six as 六小龄童
  participant p as people

  p ->> six: you are sucked!

  loop every second
    six ->> p: 不要章口就来!
  end
```

**Example:**

```mermaid
sequenceDiagram
  participant six as 六小龄童
  participant p as people

  p ->> six: you are sucked!

  loop every second
    six ->> p: 不要章口就来!
  end
```

<div id='a&o'>

### Alt and Opt

**Alt:** 
>**if** I am a boy: I love girl
**else if** I am a girl: I love boy

**Opt:**
> You can choose to do that

```mermaid
sequenceDiagram
  participant six as 六小龄童
  participant p as people

  alt people hate 六小龄童
    p ->> six: you are sucked!
    loop every second
      six ->> p: 不要章口就来!
    end

  else people love 六小龄童
    p ->> six: you are wondeful!
    loop every second
      six ->> p: 谢谢！
    end
  end
  
  opt If people say like that...
    loop evert second
      p ->> six: I love you
    end
    six ->>p: why you repeat the same thing?
  end
```

<div id='style_2'>

### Style

I don't want to write it, if you want to study it, clicking the URL under this line:

[About Style in sequenceGraph](https://mermaidjs.github.io/sequenceDiagram.html)

<div id='gant'>

## Gant Grahph

> A kind of graph to illustrates a project schedule
WARNING: In my opinion, It's actually not convenient to use gant graph by mermaid, so, If you do not have some special requirment on it, just skip this chapter.

```md
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD

    section Section
    A task           :crit,a1, 2014-01-01, 30d
    Another task     :after a1  , 20d

    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```

**Example:**
```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :crit,a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```

<div id='hr'>

### Head rules

```md
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
```
**title:** title of the graph
**dateFormat:** the format of the date

<div id='mr'>

### Middle rules

```md
   section Section
      ....

    section Another
      ....
```

**section sectionName:** declare the section

<div id='cp'>

### Content part

```md
       Completed task            :done,    des1, 2014-01-06,2014-01-08
       Active task               :active,  des2, 2014-01-09, 3d
       Future task               :         des3, after des2, 5d
       Future task2              :         des4, after des3, 5d
       
```
**Format:**
> none = $\phi$

`Task Name` : `done/active/cruit/none`, `variable name` , `begin time` , `end time/period` 

**Example:**

`making homework  : cruit, m,2014-01-1,10d`

```mermaid
gantt
       dateFormat  YYYY-MM-DD
       title Adding GANTT diagram functionality to mermaid

       section A section
       Completed task            :done,    des1, 2014-01-06,2014-01-08
       Active task               :active,  des2, 2014-01-09, 3d
       Future task               :         des3, after des2, 5d
       Future task2              :         des4, after des3, 5d

       section Critical tasks
       Completed task in the critical line :crit, done, 2014-01-06,24h
       Implement parser and jison          :crit, done, after des1, 2d
       Create tests for parser             :crit, active, 3d
       Future task in critical line        :crit, 5d
       Create tests for renderer           :2d
       Add to mermaid                      :1d

       section Documentation
       Describe gantt syntax               :active, a1, after des1, 3d
       Add gantt diagram to demo page      :after a1  , 20h
       Add another diagram to demo page    :doc1, after a1  , 48h

       section Last section
       Describe gantt syntax               :after doc1, 3d
       Add gantt diagram to demo page      :20h
       Add another diagram to demo page    :48h
```
  


